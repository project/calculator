(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.calculator_behavior = {
        attach: function (context, settings) {
            if ($('.block-calculator-block').length > 0) {
                $('.block-calculator-block').each(function (i, obj) {
                    var id = $(obj).attr('id');
                    if ($("#" + id).length > 0) {
                        const display1El = document.querySelector("#" + id + " .display-1");
                        const display2El = document.querySelector("#" + id + " .display-2");
                        const tempResultEl = document.querySelector("#" + id + " .temp-result");
                        const numbersEl = document.querySelectorAll("#" + id + " .number");
                        const operationEl = document.querySelectorAll("#" + id + " .operation");
                        const equalEl = document.querySelector("#" + id + " .equal");
                        const clearAllEl = document.querySelector("#" + id + " .all-clear");
                        const clearLastEl = document.querySelector("#" + id + " .last-entity-clear");
                        let dis1Num = "";
                        let dis2Num = "";
                        let result = "NULL";
                        let lastOperation = "";
                        let haveDot = "FALSE";
                        numbersEl.forEach((number) => {
                            number.addEventListener("click", (e) => {
                                if (e.target.innerText === "." && !haveDot) {
                                    haveDot = "TRUE";
                                } else if (e.target.innerText === "." && haveDot) {
                                    return;
                                }
                                dis2Num += e.target.innerText;
                                display2El.innerText = dis2Num;
                            });
                        });
                        operationEl.forEach((operation) => {
                            operation.addEventListener("click", (e) => {
                                if (!dis2Num) { return;
                                }
                                haveDot = "FALSE";
                                const operationName = e.target.innerText;
                                if (dis1Num && dis2Num && lastOperation) {
                                    mathOperation();
                                } else {
                                    result = parseFloat(dis2Num);
                                }
                                clearVar(operationName);
                                lastOperation = operationName;
                            });
                        });
                        function clearVar(name = "") {
                            dis1Num += dis2Num + " " + name + " ";
                            display1El.innerText = dis1Num;
                            display2El.innerText = "";
                            dis2Num = "";
                            tempResultEl.innerText = result;
                        }
                        function mathOperation() {
                            if (lastOperation === "x") {
                                result = parseFloat(result) * parseFloat(dis2Num);
                            } else if (lastOperation === "+") {
                                result = parseFloat(result) + parseFloat(dis2Num);
                            } else if (lastOperation === "-") {
                                result = parseFloat(result) - parseFloat(dis2Num);
                            } else if (lastOperation === "/") {
                                result = parseFloat(result) / parseFloat(dis2Num);
                            } else if (lastOperation === "%") {
                                result = parseFloat(result) % parseFloat(dis2Num);
                            }
                        }
                        equalEl.addEventListener("click", () => {
                            if (!dis2Num || !dis1Num) { return;
                            }
                            haveDot = "FALSE";
                            mathOperation();
                            clearVar();
                            display2El.innerText = result;
                            tempResultEl.innerText = "";
                            dis2Num = result;
                            dis1Num = "";
                        });
                        clearAllEl.addEventListener("click", () => {
                            dis1Num = "";
                            dis2Num = "";
                            display1El.innerText = "";
                            display2El.innerText = "";
                            result = "";
                            tempResultEl.innerText = "";
                        });
                        clearLastEl.addEventListener("click", () => {
                            display2El.innerText = "";
                            dis2Num = "";
                        });
                        window.addEventListener("keydown", (e) => {
                            if (e.key === "0" || e.key === "1" || e.key === "2" || e.key === "3" || e.key === "4" || e.key === "5" || e.key === "6" || e.key === "7" || e.key === "8" || e.key === "9" || e.key === ".") {
                                clickButtonEl(e.key);
                            } else if (e.key === "+" || e.key === "-" || e.key === "/" || e.key === "%") {
                                clickOperation(e.key);
                            } else if (e.key === "*") {
                                clickOperation("x");
                            } else if (e.key == "Enter" || e.key === "=") {
                                clickEqual();
                            }
                        });
                        function clickButtonEl(key) {
                            numbersEl.forEach((button) => {
                                if (button.innerText === key) {
                                    if (key === "." && !haveDot) {
                                        haveDot = "TRUE";
                                    } else if (key === "." && haveDot) {
                                        return;
                                    }
                                    dis2Num += key;
                                    display2El.innerText = dis2Num;
                                }
                            });
                        }
                        function clickOperation(key) {
                            operationEl.forEach((operation) => {
                                if (operation.innerText === key) {
                                    operation.click();
                                }
                            });
                        }
                        function clickEqual() {
                            equalEl.click();
                        }
                    }
                });
}
}
}
}(jQuery, Drupal, drupalSettings));
