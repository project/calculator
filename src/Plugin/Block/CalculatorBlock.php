<?php

namespace Drupal\calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Block to display last visited pages you visited on the website.
 *
 * @Block(
 *   id = "calculator_block",
 *   admin_label = @Translation("Calculator Block")
 * )
 */
class CalculatorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->configuration['layout'] = 1;
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $content = [];

    // Calculator display layout.
    $layout = $this->configuration['layout'] ?: 1;
    $content['layout'] = $layout;
    $build = [];
    $build['calculator'] = [
      '#theme' => 'calculator_block',
      '#content' => $content,
    ];
    $build['#attached']['library'][] = 'calculator/calculator.frontend';
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Calculator Layout'),
      '#description' => $this->t('Select the available calculator layouts.'),
      '#default_value' => $config['layout'] ?? '1',
      '#options' => [
        '1' => $this->t('Layout 1'),
        '2' => $this->t('Layout 2'),
        '3' => $this->t('Layout 3'),
        '4' => $this->t('Layout 4'),
        '5' => $this->t('Layout 5'),
        '6' => $this->t('Layout 6'),
        '7' => $this->t('Layout 7'),
        '8' => $this->t('Layout 8'),
        '9' => $this->t('Layout 9'),
        '10' => $this->t('Layout 10'),
      ],
      '#attributes' => ['class' => ['calculator-layout']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['layout'] = $form_state->getValue('layout');
  }

  /**
   * {@inheritdoc}
   *
   * Disable block cache to keep it the Calculator Block update.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
